import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
# ~ from matplotlib.colors import BoundaryNorm
# ~ from matplotlib.ticker import MaxNLocator

# ~ from matplotlib import gridspec
# ~ import matplotlib.image as mpimg

# This scripts contains: 1 class
# + class : Husimi

class Husimi:
	# The Husimi class provides a tool to generate Husimi representation
	# of wavefunctions. It is build from a grid, so you can generate 
	# representation of differents wave functions from a single object
	
	def __init__(self, grid, scale=1,pmax=2*np.pi):
		self.grid=grid
		self.scale=scale 
		# Husimi grid is defined over coherent states, but you can  
		# fix an higher resolution by changing 'scale'
		self.h=grid.h
		self.N=grid.N
		
		# Sigma of a coherent state with 1:1 aspect ratio
		self.sigmap=np.sqrt(self.h/2.0)
		self.sigmax=np.sqrt(self.h/2.0)
		
		# Boundaries for plotting
		self.xmax=grid.xmax
		self.pmax=pmax
		
		# Building Husimi grid
		# ~ self.Nx=2*int(self.xmax/self.sigmax*self.scale)+1
		self.Nx=self.N
		# ~ self.x=np.linspace(-self.xmax/2,self.xmax/2,self.Nx,endpoint=False)
		self.x=np.linspace(-self.xmax/2,self.xmax/2,self.Nx)
		
		# ~ self.x=np.linspace(0,self.xmax,self.Nx)
		# ~ self.x=self.x*(self.x<self.xmax/2)+(self.x-self.xmax)*(self.x>=self.xmax/2)
		
		# ~ self.Np=2*int(self.pmax/self.sigmap*self.scale)+1
		self.Np=self.N
		self.p=np.linspace(-self.pmax/2.0,self.pmax/2.0,self.Np)
		
		self.dx=self.x[1]-self.x[0]
		self.dp=self.p[1]-self.p[0]
		
		# ~ self.Nx+=1
		# ~ self.x=np.linspace(-self.xmax/2,self.xmax/2+self.dx,self.Nx,endpoint=False)
		
		# ~ self.Np+=2
		# ~ self.p=np.append(np.array([-self.pmax/2.0-self.dp]),self.p)
		# ~ self.p=np.append(self.p,np.array([self.pmax/2.0+self.dp]))
		
		self.husimi=np.zeros((self.Np,self.Nx))
		
		# The following is a trick : as we are working on a periodic 
		# grid, we want
		self.pshift=np.zeros((self.Np,self.N),dtype=np.complex_)
		pgrid=np.fft.fftshift(grid.p)#-self.dp*0.5
		for ip in range(0,self.Np):
			i0=int((self.p[ip]+self.N*self.h/2.0)/self.h)
			for i in range(0,self.N):
				self.pshift[ip][i]=pgrid[i]
				if i< i0-self.N/2:
					self.pshift[ip][i]=pgrid[i]+self.N*self.h
				if i> i0+self.N/2:
					self.pshift[ip][i]=pgrid[i]-self.N*self.h	

	def compute(self,wf,datafile=""):
		
		# Computes Husimi representation of a given wavefunction
		# It returns a 1-normalized 2D-density
			
		psip=np.fft.fftshift(wf.p)
		for ip in range(0,self.Np):	
			p0=self.p[ip]
			phi1=np.exp(-(self.pshift[ip]-p0)**2/(4*self.sigmap**2))
			for ix in range(0,self.Nx):
				# ~ phi=phi1*np.exp(-(1j/self.h)*(self.x[ix]+self.xmax/2-0.5*self.dx)*(self.pshift[ip]))
				phi=phi1*np.exp(-(1j/self.h)*(self.x[ix]+self.xmax/2-0.5*self.dx)*(self.pshift[ip]))
				self.husimi[ip][ix]= abs(sum(np.conj(phi)*psip))**2
		self.husimi=self.husimi
		
		if datafile!="":
			np.savez(datafile,"w",x=self.x,p=self.p,husimi=self.husimi)
	
		
	def savePNG(self,datafile='',PPfile='',dX=2*np.pi,dP=4,pngfile='husimi'):
		if datafile!="":
			data=np.load(datafile+".npz")
			husimi=data['husimi']
			x=data['x']
			p=data['p']
			data.close()
		else:
			x=self.x
			p=self.p
			husimi=self.husimi
		
		plt.clf()
		ax=plt.gca()
		ax.set_aspect('equal')
		ax.set_ylim(-dP/2,dP/2)
		ax.set_xlim(-dX/2,dX/2)
		
		ax.set_xlabel("Position")
		ax.set_ylabel("Vitesse")
		# ~ ax.set_xticks([])
		# ~ ax.set_yticks([])
		
		ax.grid(zorder=12)
		
		cmap = plt.get_cmap("Reds")
		
		if PPfile!="":
		
			img=mpl.image.imread(PPfile+".png")
			ax.imshow(img,extent=[-dX/2,dX/2,-dP/2, dP/2])
			
			plt.contourf(x,p,husimi, 25, cmap='Spectral_r',alpha=0.8,antialiased=True)

		else:
			plt.contourf(x,p,husimi, 15, cmap='Spectral_r',antialiased=True)
			

		ax.grid()
		
		# ~ plt.show()
		plt.savefig(pngfile,dpi=150)
		
	def plot(self,datafile='',PPfile='',dX=2*np.pi,dP=4,pngfile='husimi',p0=0,cm="Reds"):
		if datafile!="":
			data=np.load(datafile+".npz")
			husimi=data['husimi']
			x=data['x']
			p=data['p']
			data.close()
		else:
			x=self.x
			p=self.p
			husimi=self.husimi
			
		fig, ax = plt.subplots(figsize=(dX,dP),frameon=False)
		plt.clf()
		ax=plt.gca()
		ax.set_aspect('equal')
		ax.set_ylim(-dP/2,dP/2)
		ax.set_xlim(-dX/2,dX/2)
		
		ax.set_xlabel("Position")
		ax.set_ylabel("Vitesse")
		# ~ ax.set_xticks([])
		# ~ ax.set_yticks([])
		
		
		
		cmap = plt.get_cmap("Reds")
		
		if PPfile!="":
		
			img=mpl.image.imread(PPfile+".png")
			ax.imshow(img,extent=[-dX/2,dX/2,-dP/2+p0, dP/2+p0])
			
			levels=np.logspace(-2,0,15)*np.max(husimi)
			
			plt.contourf(x,p,husimi, levels,cmap=cm,norm=mpl.colors.LogNorm(),alpha=0.8,antialiased=True)
			
			# ~ plt.contour(x,p,husimi, levels,colors="black",antialiased=True)
			
		else:
			plt.contourf(x,p,husimi, 15, cmap=cm,antialiased=True,alpha=0.8)
			

		ax.grid(zorder=12)
		
			


		fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
		# ~ plt.savefig(pngfile,dpi=500)
		
		plt.show()
		# ~ plt.savefig(pngfile,dpi=150)
	
